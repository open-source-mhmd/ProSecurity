//
//  Profile.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 02/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import RealmSwift

class Profile: Object {

    @objc dynamic var userPseudo: String = ""
    @objc dynamic var userEmail: String = ""
    @objc dynamic var userPassword: String = ""
    
    override static func primaryKey() -> String? {
        return "userPseudo"
    }
    
}
