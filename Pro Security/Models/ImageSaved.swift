//
//  ImageSaved.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 03/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import RealmSwift

class ImageSaved: Object {

    @objc dynamic var imageSaved: NSData?
    
}
