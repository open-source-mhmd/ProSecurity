//
//  PictureDetailViewController.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 03/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import UIKit

class PictureDetailViewController: UIViewController {

    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var imageView: UIImageView!
    
    var imageS: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = imageS
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
