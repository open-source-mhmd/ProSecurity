//
//  UserProfileViewController.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 03/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import Eureka
import GenericPasswordRow
import RealmSwift

class UserProfileViewController: FormViewController {
    
    var profile: Profile? = nil
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        
        let newProfile = Profile()
        
        if let pseudo = self.form.rowBy(tag: "pseudo") as? TextRow {
            if pseudo.value == "" {
                alertMissingData()
                return
            }
            newProfile.userPseudo = pseudo.value!
        }
        if let password = self.form.rowBy(tag: "password") as? GenericPasswordRow {
            if password.value == "" {
                alertMissingData()
                return
            }
            newProfile.userPassword = password.value!
        }
        if let email = self.form.rowBy(tag: "email") as? TextRow {
            if email.value == "" {
                alertMissingData()
                return
            }
            newProfile.userEmail = email.value!
        }
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(newProfile, update: true)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func alertMissingData() {
        let alert = UIAlertController(title: "Alert", message: "Verify Input", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = try! Realm()
        self.profile = realm.objects(Profile.self).first
        self.title = "Profile"
        form +++ Section("")
            <<< TextRow(){
                $0.title = ""
                $0.placeholder = "Pseudo"
                $0.tag = "pseudo"
                $0.value = self.profile?.userPseudo
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            
            <<< GenericPasswordRow(){
                $0.value = self.profile?.userPassword
                $0.tag = "password"
            }
            
            <<< TextRow(){
                $0.title = ""
                $0.placeholder = "Email"
                $0.tag = "email"
                $0.value = self.profile?.userEmail
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnChangeAfterBlurred
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
