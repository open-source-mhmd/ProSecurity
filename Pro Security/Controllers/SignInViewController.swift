//
//  SignInViewController.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 02/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import Eureka
import RealmSwift

private let HomeFromSignInSegue = "HomeFromSignInSegue"

class SignInViewController: FormViewController {
    
    @IBAction func signInAction(_ sender: Any) {
        
        var pseudoValue = ""
        var passwordValue = ""
        
        if let pseudo = self.form.rowBy(tag: "pseudo") as? TextRow {
            if pseudo.value == "" {
                alertMissingData()
                return
            }
            pseudoValue = pseudo.value!
        }
        if let password = self.form.rowBy(tag: "password") as? PasswordRow {
            if password.value == "" {
                alertMissingData()
                return
            }
            passwordValue = password.value!
        }
        
        let realm = try! Realm()

        let profile = realm.objects(Profile.self).first
        
        if profile?.userPseudo == pseudoValue && profile?.userPassword == passwordValue {
            performSegue(withIdentifier: HomeFromSignInSegue, sender: nil)
        } else {
            alertMissingData()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Hello"
        form +++ Section("")
            <<< TextRow(){
                $0.title = ""
                $0.placeholder = "Pseudo"
                $0.tag = "pseudo"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            
            <<< PasswordRow(){
                $0.title = ""
                $0.placeholder = "Password"
                $0.tag = "password"
        }
    }
    
    func alertMissingData() {
        let alert = UIAlertController(title: "Alert", message: "Verify Input", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
