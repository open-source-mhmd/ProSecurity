//
//  SignUpViewController.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 01/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import Eureka
import GenericPasswordRow
import RealmSwift

private let HomeFromSignUpSegue = "HomeFromSignUpSegue"

class SignUpViewController: FormViewController {
    
    @IBAction func signUpAction(_ sender: Any) {
        let newProfile = Profile()
        
        if let pseudo = self.form.rowBy(tag: "pseudo") as? TextRow {
            if pseudo.value == "" {
                alertMissingData()
                return
            }
            newProfile.userPseudo = pseudo.value!
        }
        if let password = self.form.rowBy(tag: "passwordXY") as? GenericPasswordRow {
            if password.value == "" {
                alertMissingData()
                return
            }
            newProfile.userPassword = password.value!
        }
        if let email = self.form.rowBy(tag: "emailXY") as? TextRow {
            if email.value == "" {
                alertMissingData()
                return
            }
            newProfile.userEmail = email.value!
            
        }
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(newProfile, update: false)
        }
        
        performSegue(withIdentifier: HomeFromSignUpSegue, sender: nil)
    }
    
    func alertMissingData() {
        let alert = UIAlertController(title: "Alert", message: "Verify Input", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Welcome"
        form +++ Section("")
            <<< TextRow(){
                $0.title = ""
                $0.placeholder = "Pseudo"
                $0.tag = "pseudo"
            }
            
            <<< GenericPasswordRow(){
                $0.tag = "passwordXY"
            }
            
            <<< TextRow(){
                $0.title = ""
                $0.placeholder = "Email"
                $0.tag = "emailXY"
                }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
