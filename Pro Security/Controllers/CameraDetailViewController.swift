//
//  CameraDetailViewController.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 03/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import UIKit
import RealmSwift

class CameraDetailViewController: UIViewController {

    @IBOutlet var movieView: UIView!
    var URI: String?
    var mediaPlayer = VLCMediaPlayer()
    
    @IBAction func takePictureAction(_ sender: Any) {
        self.takePicture()
    }
    
    @IBAction func recordVideoAction(_ sender: Any) {
    }

    override func viewDidAppear(_ animated: Bool) {
        // Associate the movieView to the VLC media player
        super.viewDidAppear(animated)
        mediaPlayer.drawable = self.movieView
        
        // Create `VLCMedia` with the URI retrieved from the camera
        if let URI = URI {
            let url = URL(string: URI)
            let media = VLCMedia(url: url!)
            mediaPlayer.media = media
        }
        
        mediaPlayer.play()
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //mediaPlayer.stop()
    }
    
    private func takePicture() {
        let renderer = UIGraphicsImageRenderer(size: self.movieView.bounds.size)
        let imageCaptured = renderer.image { ctx in
            self.movieView.drawHierarchy(in: self.movieView.bounds, afterScreenUpdates: true)
        }
        let data = NSData(data: UIImagePNGRepresentation(imageCaptured)!)
        let newImageSaved = ImageSaved()
        newImageSaved.imageSaved = data
        let realm = try! Realm()
        try! realm.write {
            realm.add(newImageSaved, update: false)
        }

    }

}
